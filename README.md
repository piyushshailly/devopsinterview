### DevOps technical assignment

#### This Project contents One Jenkinsfile and 2 Project Folders 

- [Airport ReadMe](./airport/README.md)

- [Countries ReadMe](./countries/README.md)

* NOTE: You need to check ports of nginx (if already not in use)

* [Docker Compose file](./airport/docker-compose.yml)

```
 ports:
      - "8083:80"
```