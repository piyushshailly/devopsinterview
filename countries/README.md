### Build using Docker 

#### Build Docker Image using ```docker build``` and tag image  

```
docker build -t countries:1.0.1 .
```

#### Now Run Docker Image and expose it to 8000 for outer world

```
docker run -itdp 8082:8080 countries:1.0.1
```
#### Build Using Docker-compose###

```docker-compose up -d```

#### Scale using docker-compose

```docker-compose up --scale countries=2 -d```

#### Check Liveness

```curl localhost:8082/health/live```
