### Build using Docker 

#### Build Docker Image using ```docker build``` and tag image  

```
docker build -t airport:1.0.1 .
```

#### Now Run Docker Image and expose it to 8000 for outer world

```
docker run -itdp 8083:8080 airport:1.0.1
```
#### Build Using Docker-compose###

```docker-compose up -d```

#### Scale using docker-compose

```docker-compose up --scale airport=2 -d```

#### Check Liveness

```curl localhost:8083/health/live```
